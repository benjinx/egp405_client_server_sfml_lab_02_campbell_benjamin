#include <SFML/Graphics.hpp>
#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include <SFML/Window/Mouse.hpp>
#include <iostream>

bool launchServer(RakNet::RakPeerInterface* server, int port)
{
	server->SetIncomingPassword(0, 0);

	// IPV4 socket
	RakNet::SocketDescriptor  sd;
	sd.port = port;
	sd.socketFamily = AF_INET;

	if (server->Startup(4, &sd, 1) != RakNet::RAKNET_STARTED)
	{
		printf("\nFailed to start server with IPV4 ports");
		return false;
	}

	server->SetOccasionalPing(true);
	server->SetUnreliableTimeout(1000);
	server->SetMaximumIncomingConnections(4);

	printf("\nSERVER IP addresses:");
	for (unsigned int i = 0; i < server->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("\n%i. %s (LAN=%i)", i + 1, sa.ToString(false), sa.IsLANAddress());
	}

	return true;
}

bool launchClient(RakNet::RakPeerInterface* client, const char* serverIP, int serverPort, int clientPort)
{
	// IPV4 socket
	RakNet::SocketDescriptor sd(clientPort, 0);
	sd.socketFamily = AF_INET;

	client->Startup(8, &sd, 1);
	client->SetOccasionalPing(true);

	if (client->Connect(serverIP, serverPort, 0, 0) !=
		RakNet::CONNECTION_ATTEMPT_STARTED)
	{
		printf("\nAttempt to connect to server FAILED");
		return false;
	}

	printf("\nCLIENT IP addresses:");
	for (unsigned int i = 0; i < client->GetNumberOfAddresses(); i++)
	{
		printf("\n%i. %s\n", i + 1, client->GetLocalIP(i));
	}

	return true;
}

void printUsage()
{
	printf("\n-- USAGE --");
	printf("\nTicTacToe <serverPort>");
	printf("\nTicTacToe <serverIP> <serverPort> [clientPort]\n");
}

void updateRakNet(RakNet::RakPeerInterface* peer)
{
	RakNet::Packet* packet;
	for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
	{
		switch (packet->data[0])
		{
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			printf("Another client has disconnected.\n");
			break;
		case ID_REMOTE_CONNECTION_LOST:
			printf("Another client has lost the connection.\n");
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			printf("Another client has connected.\n");
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
			printf("Our connection request has been accepted.\n");
			break;
		case ID_NEW_INCOMING_CONNECTION:
			printf("A connection is incoming.\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			printf("The server is full.\n");
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			//if (isServer){
			//	printf("A client has disconnected.\n");
			//}
			//else {
			//	printf("We have been disconnected.\n");
			//}
			break;
		case ID_CONNECTION_LOST:
			//if (isServer){
			//	printf("A client lost the connection.\n");
			//}
			//else {
			//	printf("Connection lost.\n");
			//}
			break;
		default:
			printf("Message with identifier %i has arrived.\n", packet->data[0]);
			break;
		}
	}
}

int main(int argc, char** argv)
{
	RakNet::RakPeerInterface* peer = RakNet::RakPeerInterface::GetInstance();
	bool isServer;


	if (argc == 2)
	{
		const int portNum = atoi(argv[1]);
		if (launchServer(peer, portNum) == false)
			return 0;
		isServer = true;
	}
	else if (argc == 3 || argc == 4) // ServerIP serverport [clientPort]
	{
		const char* serverIP = argv[1];
		const int serverPort = atoi(argv[2]);
		const int clientPort = argc == 4 ? atoi(argv[3]) : serverPort + 1;

		if (launchClient(peer, serverIP, serverPort, clientPort) == false)
			return 0;
		isServer = false;
	}
	else
	{
		printUsage();
		system("pause");
		return 0;
	}

	//create the window
	sf::RenderWindow window(sf::VideoMode(800, 600), "My window");

	// run the program as long as the window is open
	while (window.isOpen())
	{
		updateRakNet(peer);

	

		sf::Mouse* mouse = new sf::Mouse;
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				window.close();
		}

		// clear the window with black color
		window.clear(sf::Color::Black);

		// draw everything here...

		// define a triangle
		sf::CircleShape triangle(80, 3);
		
		triangle.setFillColor(sf::Color(100, 250, 50));

 
		triangle.setPosition((float)mouse->getPosition(window).x - 40, (float)mouse->getPosition(window).y - 40);
		std::cout << mouse->getPosition(window).x << ", " << mouse->getPosition(window).y << std::endl;
		window.draw(triangle);

		// end the current frame
		window.display();
		//delete mouse;
		//mouse = nullptr;
	}
		
	return 0;
}